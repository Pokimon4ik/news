from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import News, Comments


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['name', 'content']


class NewsTagsForm(NewsForm):
    tagss = forms.CharField(max_length=20, required=False)

    class Meta(NewsForm.Meta):
        fields = NewsForm.Meta.fields + ['tagss', ]


class CommentsFormAnonym(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['user_name', 'text', 'news']
        widgets = {'news': forms.HiddenInput()}


class CommentsFormRegist(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['user_name', 'text', 'news']
        widgets = {'news': forms.HiddenInput()}


class RegisterForm(UserCreationForm):
    phone = forms.CharField(max_length=20, help_text='Телефон')
    city = forms.CharField(max_length=100, help_text='Город')

    class Meta:
        model = User
        fields = ('username', 'phone', 'city', 'password1', 'password2',)


class FindTagsForm(forms.Form):
    date = forms.DateTimeField(input_formats=['%Y-%m-%d'], required=False)
    tags = forms.CharField(required=False)
