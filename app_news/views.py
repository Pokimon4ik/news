from datetime import datetime

from django import forms
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.contrib.auth.views import LoginView, LogoutView
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import FormMixin, CreateView, UpdateView

from .models import News, Profile, Tag
from .forms import CommentsFormRegist, RegisterForm, NewsTagsForm, FindTagsForm


class NewsListView(ListView):
    model = News
    template_name = 'news_list.html'
    context_object_name = 'news_list'
    form_class = FindTagsForm

    def get_queryset(self):
        queryset = super(NewsListView, self).get_queryset().all()
        if 'tag' in self.request.GET:
            tag = self.request.GET.get('tag')
            tag_id = Tag.objects.get(name=tag) if tag is not None else None
            queryset = queryset.filter(tags=tag_id)
        if 'date' in self.request.GET:
            date = self.request.GET.get('date')
            date_datetime = datetime.strptime(date, '%Y-%m-%d') if date is not None else None
            queryset = queryset.filter(updated_at__date=date_datetime)
        return queryset


class NewsCreate(PermissionRequiredMixin, CreateView):
    permission_required = 'app_news.view_news'
    form_class = NewsTagsForm
    template_name = 'app_news/create.html'

    def form_valid(self, form):
        news = form.save()
        tags_array = form.cleaned_data['tagss'].split(' ')
        for tag in tags_array:
            cur_tag, is_create = Tag.objects.get_or_create(name=tag)
            news.tags.add(cur_tag)
        return super(NewsCreate, self).form_valid(form)


class NewsFormEdit(UpdateView):
    form_class = NewsTagsForm
    template_name = 'app_news/edit.html'
    queryset = News.objects.all()

    def get_object(self):
        id_ = self.kwargs.get('id')
        return get_object_or_404(News, id=id_)

    def form_valid(self, form):
        news = form.save()
        news.updated_at = datetime.now()
        tags_array = form.cleaned_data['tagss'].split(' ')
        for tag in tags_array:
            cur_tag, is_create = Tag.objects.get_or_create(name=tag)
            news.tags.add(cur_tag)
        return super().form_valid(form)


class NewsDetailFormView(FormMixin, DetailView):
    template_name = 'app_news/news_detail.html'
    model = News
    form_class = CommentsFormRegist

    def get_success_url(self):
        return reverse('news-detail', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super(NewsDetailFormView, self).get_context_data(**kwargs)
        form = CommentsFormRegist(initial={
            'post': self.object,
            'news': self.object,
        })
        if self.request.user.is_authenticated:
            form.fields['user_name'] = forms.CharField(widget=forms.HiddenInput(), initial='None')
        context['comment_form'] = form
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        obj = form.save(commit=False)
        if self.request.user.is_authenticated:
            obj.author = self.request.user
            obj.user_name = self.request.user.username
        else:
            obj.author = None
        obj.save()
        return super(NewsDetailFormView, self).form_valid(obj)


class MyLoginView(LoginView):
    template_name = 'users/login.html'


class MyLogoutView(LogoutView):
    next_page = reverse_lazy('news')


class RegisterView(CreateView):
    form_class = RegisterForm
    template_name = 'users/register.html'

    def form_valid(self, form):
        user = form.save()
        phone = form.cleaned_data.get('phone')
        city = form.cleaned_data.get('city')
        Profile.objects.create(
            user=user,
            city=city,
            phone=phone,
        )
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        group = Group.objects.get(name='Пользователи')
        group.user_set.add(user)
        user = authenticate(username=username, password=raw_password)
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return super(RegisterView, self).form_valid(form)

    def get_success_url(self):
        return reverse('news')


class ProfileView(PermissionRequiredMixin, TemplateView):
    permission_required = 'app_news.can_visit_profile'
    template_name = 'users/profile.html'
