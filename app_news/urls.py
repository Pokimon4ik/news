from django.urls import path
from .views import (
    NewsListView,
    NewsDetailFormView,
    MyLoginView,
    MyLogoutView,
    RegisterView,
    ProfileView,
    NewsCreate, NewsFormEdit)

urlpatterns = [
    path('news/', NewsListView.as_view(), name='news'),
    path('news/<int:id>/edit/', NewsFormEdit.as_view(), name='news-edit'),
    path('news/<int:pk>/', NewsDetailFormView.as_view(), name='news-detail'),
    path('news/create/', NewsCreate.as_view(), name='news-create'),
    path('login/', MyLoginView.as_view(), name='login'),
    path('logout/', MyLogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/', ProfileView.as_view(), name='profile'),
]
