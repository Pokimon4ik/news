from django.contrib import admin
from django.contrib.auth.models import Group
from django.db import models
from django.forms import TextInput, Textarea

from .models import Comments, News, Profile, Tag


class CommentsInLine(admin.TabularInline):
    model = Comments


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'updated_at', 'active', ]
    list_filter = ['active']
    inlines = [CommentsInLine]

    actions = ['mark_as_active', 'mark_as_inactive']

    def mark_as_active(self, request, queryset):
        queryset.update(active=True)

    def mark_as_inactive(self, request, queryset):
        queryset.update(active=False)

    mark_as_active.short_description = 'Активировать запись'
    mark_as_inactive.short_description = 'Деактивировать запись'


@admin.register(Comments)
class CommentsAdmin(admin.ModelAdmin):
    list_display = ['user_name', 'short_comment']
    list_filter = ['user_name']

    actions = ['delete_admin']

    def delete_admin(self, request, queryset):
        queryset.update(text='Удалено администратором')

    delete_admin.short_description = 'Удалить админом'

    def short_comment(self, obj):
        result = obj.text[:16]
        if len(result) > 15:
            result += "..."
        return result

    short_comment.short_description = 'Короткий коментарий'


@admin.register(Tag)
class Tag(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Profile)
class Profile(admin.ModelAdmin):
    list_display = ['user', 'phone', 'city', 'verification']

    actions = ['mark_as_verified', 'mark_as_unverified']

    def mark_as_verified(self, request, queryset):
        for profile in queryset:
            group = Group.objects.get(name='Верифицированные')
            group.user_set.add(profile.user)
        queryset.update(verification=True)

    def mark_as_unverified(self, request, queryset):
        for profile in queryset:
            group = Group.objects.get(name='Верифицированные')
            group.user_set.remove(profile.user)
        # group = Group.objects.get(name='Верифицированные')
        # group.user_set.remove(request.user)
        queryset.update(verification=False)

    mark_as_verified.short_description = 'Верифицировать'
    mark_as_unverified.short_description = 'Убрать верификацию'
