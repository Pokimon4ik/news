from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Tag(models.Model):
    name = models.CharField(max_length=40, verbose_name='тэг')


class News(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    content = models.TextField(verbose_name='Содержание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата редактирования')
    active = models.BooleanField(default=False, verbose_name='Активность')
    tags = models.ManyToManyField(Tag, null=True, verbose_name='Тэги', related_name='news')

    def get_absolute_url(self):
        return reverse('news')

    class Meta:
        ordering = ['-created_at']


class Comments(models.Model):
    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
        related_name='comments'
    )
    user_name = models.CharField(max_length=100, verbose_name='Имя пользователя')
    text = models.TextField(verbose_name='Комментарий')
    news = models.ForeignKey(News, null=True, on_delete=models.CASCADE, verbose_name='Новость', related_name='comments')


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    phone = models.CharField(max_length=20, verbose_name='Телефон')
    city = models.CharField(max_length=100, verbose_name='Город')
    verification = models.BooleanField(default=False, verbose_name='Верификации')

    def count_news(self):
        pass

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
        permissions = (
            ("can_publish", "Может публиковать"),
            ("can_visit_profile", "Может зайти в личный кабинет"),
        )
