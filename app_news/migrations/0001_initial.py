# Generated by Django 2.2 on 2021-01-03 10:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('content', models.TextField(verbose_name='Содержание')),
                ('created_at', models.DateTimeField(auto_now=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата редактирования')),
                ('active', models.BooleanField(verbose_name='Активность')),
            ],
            options={
                'order_with_respect_to': 'created_at',
            },
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_name', models.CharField(max_length=100, verbose_name='Имя пользователя')),
                ('text', models.TextField(verbose_name='Комментарий')),
                ('news', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_news.News', verbose_name='Новосить')),
            ],
        ),
    ]
